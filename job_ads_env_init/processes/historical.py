from job_ads_env_init.configurators import opensearchConfigurator, k8sConfigurator
from job_ads_env_init.processes import subprocesses


def historical_setup(os_configurator: opensearchConfigurator,
                     k8s_configurator=k8sConfigurator.KubernetesConfigurator()):
    # Importer setup
    os_configurator.create_role("historical-write-role",
                                ["cluster_monitor", "indices:data/write/bulk"],
                                [opensearchConfigurator.IndexPermission(["historical*"], ["manage", "indices_all"]),
                                 opensearchConfigurator.IndexPermission(["*"], ["indices:admin/aliases/get"])])
    subprocesses.create_os_account_and_k8s_secret(k8s_configurator,
                                                  os_configurator,
                                                  "historical-write",
                                                  ["historical-write-role"],
                                                  "historical-write",
                                                  "ES_USER",
                                                  "ES_PWD")

    # Service setup
    os_configurator.create_role("historical-read-role",
                                ["cluster_monitor", "indices:data/read/scroll", "indices:data/read/scroll/clear"],
                                [opensearchConfigurator.IndexPermission(["historical*", "jae-synonym-dictionary*"], ["read", "search"])])
    subprocesses.create_os_account_and_k8s_secret(k8s_configurator,
                                                  os_configurator,
                                                  "historical-read",
                                                  ["historical-read-role"],
                                                  "historical-read",
                                                  "ES_USER",
                                                  "ES_PWD")


    # TODO: upload synonym dictionary
