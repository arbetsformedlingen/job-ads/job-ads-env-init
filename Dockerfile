FROM docker.io/library/python:3.11.10-slim-bookworm

COPY . /app
WORKDIR /app

RUN python -m pip install --upgrade pip setuptools build && \
    python -m build --outdir /app/out



FROM docker.io/library/python:3.11.10-slim-bookworm

ENV APP_VER=0.1.0

WORKDIR /app
COPY --from=0 /app/out/job_ads_env_init-${APP_VER}-py3-none-any.whl ./
RUN apt-get -y update &&\
    python -m pip install job_ads_env_init-${APP_VER}-py3-none-any.whl
